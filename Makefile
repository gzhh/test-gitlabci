.PHONY: all build

all: build
build:
	go build -o ./dist/app -v -a -ldflags '-w -s' -tags=jsoniter main.go
