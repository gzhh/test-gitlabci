FROM --platform=$TARGETPLATFORM golang:1.21-alpine AS builder
ENV CGO_ENABLED 0
ENV GOPROXY https://goproxy.cn,direct
WORKDIR /workspace
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download
COPY . .
RUN go build -ldflags="-s -w" -a -o app ./main.go

FROM --platform=$TARGETPLATFORM alpine
WORKDIR /workspace
COPY --from=builder /workspace/app /workspace/
ENV TZ Asia/Shanghai
ENV RUN_MODE test
EXPOSE 8188
ENTRYPOINT ["/workspace/app"]